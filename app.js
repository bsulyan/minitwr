var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var mongoose = require('mongoose');


var routes = require('./routes/index');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/images/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session( { secret : '12345'} ));

app.use(function (req , res ,next){
	if (req.session.isConnect === undefined){
		req.session.isConnect = false;
		req.session.account= {};
	}
	next();
});

app.use('/', routes);
app.use('/register', routes);
app.use('/connect', routes);
app.use('/profil',routes);
app.use('/searchPosts',routes);
app.use('/nbPosts',routes);
app.use('/disconnect',routes);
app.use('/aboutUs',routes);
app.use('/hashtag',routes);

// Init mongoose
mongoose.connect('mongodb://localhost/minitwr', function(err) {
	if (err) { throw err; }
});

	var accountSchema = new mongoose.Schema({
		pseudo : { type : String, match: /^[a-zA-Z0-9-_]+$/ },
		email : { type : String , match: /^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$/i },
		pass : String ,
		prenom : { type : String, default : '' },
		nom : { type : String, default : '' },
		age : { type : Number, min : 0 ,default : 10 },
		sex : { type : String , default : '' },
		region : { type : String , default : '' },
		commentaire : { type : String , default : '' },
		date : { type : Date, default : Date.now },
		friends: { type : Array , default : []},
		posts : Array ,
		notification : { type : Array , default : []},
		imProfil : { type : String,  default :  "http://lorempixel.com/250/250/cats/4"},
		imCouv : { type : String,  default :  "http://aucklandvetdentist.co.nz/media/2076/faq_cat_banner.jpg"}
	});
	
	/*var pageSchema = new mongoose.Schema({
		idCreateur : String,
		name : String,
		posts : { type : Array , default : []},
		date : { type : Date, default : Date.now }
	});*/
	
	var postSchema = new mongoose.Schema({
		pseudo : String,
		date : { type : Date, default : Date.now },
		message : String,
                hashtag : Array,
		page : String
	});

mongoose.model('account', accountSchema);
mongoose.model('post', postSchema);
//mongoose.model('page', pageSchema);

mongoose.connection.close();

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err,
      session : req.session
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {},
    session : req.session
  });
});

app.listen(80);
module.exports = app;
