var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var sha1 = require('node-sha1');
var md5 = require('js-md5');

/* GET home page. */
router.get('/', function(req, res, next) {
		res.render('index', { session : req.session });
});


/* GET about Us page. */
router.get('/about', function(req, res, next) {
  res.render('about', { title: 'A propos', session : req.session });
});



/* GET profil page. */
router.get('/profil/:idAccount', function(req, res, next) {
	mongoose.connect('mongodb://localhost/minitwr', function(err) {
	  if (err) { throw err; }
	});

	var accountModel =mongoose.model('account') ;
	
	accountModel.findOne({ 'pseudo' : req.params.idAccount }, function (err, account){
		if (err) { throw err; }
		mongoose.connection.close();
		if (account==null) {
			next();
		}
		else {
				res.render('profil', { account: account , session : req.session });
		}
	});
});

/* GET modified profil page*/
router.get('/profil/:idAccount/modifier', function(req, res, next) {
	
	if(req.params.idAccount == req.session.account.pseudo) {
		mongoose.connect('mongodb://localhost/minitwr', function(err) {
			if (err) { throw err; }
		});	
		var accountModel =mongoose.model('account') ;
		
		accountModel.findOne({ 'pseudo' : req.params.idAccount}, function (err, account){
			if (err) { throw err; }
			if (account==null) {
				next();
			}
			else {
					mongoose.connection.close();
					res.render('modified', { account: account , session : req.session });
			}
		});
	}
	else {next()}
});

/*POST profil page*/
router.post('/profil/:idAccount', function(req, res, next) {

	if (req.session.isConnect && req.session.account.pseudo==req.params.idAccount && req.body.message != ''){
		mongoose.connect('mongodb://localhost/minitwr', function(err) {
			if (err) { throw err; }
		});
		var accountModel =mongoose.model('account') ;

		accountModel.findOne({ 'pseudo' : req.params.idAccount }, function (err, account){
			if (err) { throw err; }
			
			var postModel = mongoose.model('post');
			var post = new postModel;
			post.pseudo = req.params.idAccount;
			post.message = req.body.message;
			post.page = 'profil/'+account.pseudo;
			hashtag = post.message.match(/#(\w*[a-zA-Z^@&"()!_$*€£`;áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ+=\/:?#ê.;é%\\|<>,-]+\w*)/g);
			if (hashtag != undefined) {
				for(i=0;i<hashtag.length;i++){
					hashtag[i] = hashtag[i].substring(1,hashtag[i].length);
				}
			}

			
			post.hashtag =hashtag
			post.save(function (err , post) {
				if (err) { throw err; }
				
				account.posts.push(post.id);

				accountModel.update({pseudo : account.pseudo} , {$set :{posts : account.posts }} ,function (err) {
						if (err) { throw err; }
						mongoose.connection.close();
						res.redirect('/profil/'+req.params.idAccount);
				});
			});	
		});
	}
	else{
		res.redirect('/profil/'+req.params.idAccount);
	}
});

/* POST modified profil page*/
router.post('/profil/:idAccount/modifier', function(req, res, next) {
	
	if(req.params.idAccount == req.session.account.pseudo) {
		mongoose.connect('mongodb://localhost/minitwr', function(err) {
			if (err) { throw err; }
		});	
		var accountModel =mongoose.model('account') ;
		
		accountModel.findOne({ 'pseudo' : req.params.idAccount}, function (err, account){
			if (err) { throw err; }
			if (account==null) {
				mongoose.connection.close();
				next();
			}
			else {
				account.nom = req.body.nom;
				account.prenom = req.body.prenom;
				account.age = req.body.age;
				account.commentaire = req.body.commentaire;
				account.sex = req.body.sex;
				account.region = req.body.region;
				account.imProfil = req.body.imProfil;
				account.imCouv = req.body.imCouv;
				
				account.save(function (err, account) {
						if (err) { throw err; }		
					mongoose.connection.close();
					req.session.account = account;
					res.redirect('/profil/'+req.params.idAccount);
				});
			}
		});
	}
	else {next()}
});

/* GET hashtage page*/
router.get('/hashtag/:idHashtag', function(req, res, next) {
	var idHashtag = req.param('search');
	res.render('hashtag', { hashtag : idHashtag, session : req.session });
});

/* GET searchPosts*/
router.get('/searchPosts', function(req, res, next) {
	if (req.query.curs!== undefined && req.query.nb!== undefined){
		mongoose.connect('mongodb://localhost/minitwr', function(err) {
			if (err) { throw err; }
		});
		if( req.query.pseudo != ''){

			mongoose.model('account').findOne({ 'pseudo' : req.query.pseudo }, function (err, account){
				if (err) { throw err; }
				
				var postModel = mongoose.model('post');
				idPosts = account.posts.slice(req.query.curs-Math.min(req.query.nb,req.query.curs),req.query.curs);
				postModel.find({ '_id' : { $in : idPosts } } ,function (err , posts) {
					if (err) { throw err; }
					mongoose.connection.close();
					
					var posts2 = [];
					for (i = 0 ; i<posts.length ;i++){
						posts2.push({pseudo : posts[i].pseudo , message : posts[i].message ,hashtag : posts[i].hashtag ,date : posts[i].date});
					}

					res.set('Content-Type', 'text/html');
					res.send(JSON.stringify(posts2));
				});
			});
		}
		else if(req.query.hashtag != '' ){
			mongoose.model('post').find({ 'hashtag' : { $in : [req.query.hashtag] }}, function (err, posts){
				if (err) { throw err; }
				mongoose.connection.close();
				
				posts = posts.slice(req.query.curs-Math.min(req.query.nb,req.query.curs),req.query.curs);
				
				var posts2 = [];
				for (i = 0 ; i<posts.length ;i++){
					posts2.push({pseudo : posts[i].pseudo , message : posts[i].message ,hashtag : posts[i].hashtag , date : posts[i].date});
				}
				
				res.set('Content-Type', 'text/html');
				res.send(JSON.stringify( posts2));
			});
		}
		else {
			var postModel = mongoose.model('post');
			postModel.find(null,function (err, posts){
				mongoose.connection.close();
				
				posts = posts.slice(req.query.curs-Math.min(req.query.nb,req.query.curs),req.query.curs);
				var posts2 = [];
				for (i = 0 ; i<posts.length ;i++){
					posts2.push({pseudo : posts[i].pseudo , message : posts[i].message ,hashtag : posts[i].hashtag ,date : posts[i].date});
				}
				
				res.set('Content-Type', 'text/html');
				res.send(JSON.stringify(posts2));
			});
		}
	}
	else { next() }
});

/* GET nbPosts page */
router.use('/nbPosts',function(req, res, next){

	console.log(req.query.critere);
	if(typeof req.query.pseudo != 'undefined' && req.query.hashtag != 'undefined'  ){
		mongoose.connect('mongodb://localhost/minitwr', function(err) {
			if (err) { throw err; }
		});
		if(req.query.pseudo != '' ){
			mongoose.model('account').findOne({ 'pseudo' : req.query.pseudo }, function (err, account){
				if (err) { throw err; }
				mongoose.connection.close();
				res.set('Content-Type', 'text/html');
				res.send(''+account.posts.length);
			});
		}
		else if(req.query.hashtag != '' ){
			console.log('test');
			mongoose.model('post').find({ 'hashtag' : { $in : [req.query.hashtag] }}, function (err, posts){
				if (err) { throw err; }
				mongoose.connection.close();
				res.set('Content-Type', 'text/html');
				res.send(''+ posts.length);
			});
		}
		else {
			var postModel = mongoose.model('post');
			postModel.find(null,function (err, posts){
				mongoose.connection.close();
				res.set('Content-Type', 'text/html');
				res.send(''+posts.length);
			});
		}
	}	
	else {next()}
});

/* GET connect page. */
router.get('/connect', function(req, res, next) {
  res.render('connect', { err : '' , session : req.session });
});

/* POST connect page */
router.post('/connect', function(req, res, next) {
	mongoose.connect('mongodb://localhost/minitwr', function(err) {
	  if (err) { throw err; }
	});
	var accountModel = mongoose.model('account');
	
	accountModel.findOne({ $or: [ { 'pseudo' : req.body.user},{ 'email': req.body.user}] , 'pass' : sha1(req.body.pass) }, function(err, account){
		if (account == null){
			mongoose.connection.close();
			res.render('connect', { err : 'Le nom d\'utilisateur et/ou le mot de passe est/sont erroné(s).\n Veuillez vérifier et réessayer.' , session : req.session });
		}
		else {
			
			req.session.isConnect = true;
			req.session.account = account;
			console.log(account.pseudo+' vient de se connecter');
			mongoose.connection.close();
			res.redirect('profil/'+account.pseudo);
		}
	});
});

/* GET disconnect page*/
router.get('/disconnect', function (req , res, next){
	
	req.session.destroy(function(err) {
	});
	res.redirect('/');
});

/* GET register page. */
router.get('/register', function(req, res, next) {
  res.render('register', { err: '' , session : req.session });
});

/* POST register page. */
router.post('/register',function(req, res, next) {
	
	mongoose.connect('mongodb://localhost/minitwr', function(err) {
	  if (err) { throw err; }
	});

	var accountModel = mongoose.model('account');
	
	accountModel.findOne({ $or: [ { 'pseudo' : req.body.pseudo},{ 'email': req.body.email}] }, function(err,doc){
		if  (doc == null){
			if (( /^[a-zA-Z0-9-_]+$/ ).test(req.body.pseudo) == false){
				mongoose.connection.close();
				res.render('register', { err: 'Nom d\'utilisateur invalide' , session : req.session });
			}
			else if ((/^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$/i).test(req.body.email) == false)
			{
				mongoose.connection.close();
				res.render('register', { err: 'Email invalide' , session : req.session });
			}
			/* PASSWORD test */ 
				else if (req.body.pass.length < 8){
					mongoose.connection.close();
					res.render('register', { err: 'Le mot de passe doit avoir au minimun 8 caractères.' , session : req.session });
				}
				else if((/^(\w|¥|€|\$|£|%|&|§|#)+$/).test(req.body.pass)==false){
					mongoose.connection.close();
					res.render('register', { err: 'Le mot de passe peut contenir des lettres et doit contenir au moins un chiffre et un caractère spécial (¥,€,$,£,%,&,§ ou #)', session : req.session  });
				}
				else if ((/(¥|€|\$|£|%|&|§|#)+/).test(req.body.pass)==false) {
					mongoose.connection.close();
					res.render('register', { err: 'Le mot de passe doit contenir au moins un caratère spécial (¥,€,$,£,%,&,§ ou #)', session : req.session  });
				}	
				else if((/\d+/).test(req.body.pass)==false ){
					mongoose.connection.close();
					res.render('register', { err: 'Le mot de passe doit contenir au moins un chiffre', session : req.session  });
			}
			else {
				
				var account = new accountModel;
				account.pseudo = req.body.pseudo;
				account.email = req.body.email;
				account.pass = sha1(req.body.pass);
				account.imProfil = "http://www.gravatar.com/avatar/" + md5(req.body.email) +"?s=200&r=pg&f=y";

				var postModel = mongoose.model('post');
				var post = new postModel;
				post.pseudo = account.pseudo;
				post.message = account.pseudo + " vient de s'inscrire !! Bienvenue sur MiniTwr !!!";
				post.page = 'profil/'+account.pseudo;
				
				post.save(function (err , post) { 
					if (err) { throw err; } 
					account.posts.push(post.id);

					account.save(function (err, account) {
						if (err) { throw err; }		
						
							req.session.isConnect = true;
							req.session.account = account;
							
							console.log(account.pseudo + ': compte enregistré avec succès !');
							mongoose.connection.close();
							res.redirect('profil/'+account.pseudo);
					});
				});	
			}
		}
		else if  (doc.pseudo == req.body.pseudo){
			mongoose.connection.close();
			res.render('register', { err: 'Nom d\'utilisateur déja utilisé' , session : req.session });
		}
		else if (doc.email == req.body.email){
			mongoose.connection.close();
			res.render('register', { err: 'Email déja utilisé' , session : req.session });
		}
	});
	
});

module.exports = router;
