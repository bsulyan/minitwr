# Minitwr

> We did not want to have one of the best projects but the best

**Minitwr** is our project for validate a web programming module for our studies.
We could have stopped us too much but we wanted to deepen our work.
We were even told that we had enough to get the maximum score , but we still continued .

### Version
1.0 - A fonctionnal project

### Tech

Minitwr uses a number of open source projects to work properly:

* [Twitter Bootstrap] - For a beautiful website
* [node.js] - For make a web 2.0 you must to use it
* [Express] - For improve performance of node.js
* [mongodb] - It is essential for have a website nosql

### Installation

- You need Mongodb installed :

  + You can find it here :
  > http://www.mongodb.org/downloads?_ga=1.211305949.932219332.1430058358


- You need Node.js installed :
  + You can find it here :
  > https://nodejs.org/download/


After : 
```sh
$ git clone https://github.com/bsulyan/minitwr
$ cd minitwr
$ npm install
$ sudo -s
$ PORT=80 ./bin/www 
```

And access to http://localhost/

### Todo's

 - Add followers system 
 - System for upload pictures (actually you must to upload your images on others sites for profil image and cover image)

### Already Set Features

 - A profile for each member that he can modify by adding his description, an avatar, a cover image and others things
 - A hashtag-like system
 - A tchat page for talk with other people and read what he writes
