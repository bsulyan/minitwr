function changeTooltip(block,condition,message){
	var tooltip = document.getElementById('tooltip');
	if (condition) {
		block.className = 'correct';
		tooltip.style.display = 'none';
		return true;
	} else {
		block.className = 'incorrect';
		tooltip.innerHTML = message;
		tooltip.style.display = 'inline-block';
		return false;
	}
}
		
var check = {}; 

check['pseudo'] = function() {
	var pseudo = document.getElementById('pseudo');
	return changeTooltip(pseudo,( /^[a-zA-Z0-9-_]+$/ ).test(pseudo.value),'Nom d\'utilisateur non valide');
};

check['email'] = function() {
	var email = document.getElementById('email');
	return changeTooltip(email,(/^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$/i).test(email.value),'Adresse email non valide.');
};
check['pass'] = function() {
	var pass = document.getElementById('pass');
	if (pass.value.length >= 8){
		if((/^(\w|¥|€|\$|£|%|&|§|#)+$/).test(pass.value)){
			if ((/(¥|€|\$|£|%|&|§|#)+/).test(pass.value)) {
				if((/\d+/).test(pass.value) ){
					return changeTooltip(pass,true,'');
				}
				else {return changeTooltip(pass,false,'Le mot de passe doit contenir au moins un chiffre');}
			}
			else { return changeTooltip(pass, false ,'Le mot de passe doit contenir au moins un caractère spécial (¥,€,$,£,%,&,§ ou #)');}
		}
		else { return changeTooltip(pass,false ,'Le mot de passe peut contenir des lettres et doit contenir au moins un chiffre et un caractère spécial (¥,€,$,£,%,&,§ ou #)');}
	}
	else { return changeTooltip(pass,false,'Le mot de passe doit avoir au minimun 8 caractères.');}	
};

check['pass2'] = function() {
	var pass = document.getElementById('pass'),pass2 = document.getElementById('pass2');
	return 	changeTooltip(pass2,(pass.value == pass2.value && pass2.value != ''),'Retapez le même mot de passe.')
};
		
(function() { 

	var myForm = document.getElementById('formRegister'),
		inputs = document.querySelectorAll('input[type=text], input[type=password]');
		inputsLength = inputs.length;
	
	for (var i = 0 ; i < inputsLength ; i++) {
		inputs[i].addEventListener('keyup', function(e) {
		check[e.target.id](e.target.id); 
		}, false);
		inputs[i].addEventListener('focus', function(e) {
		check[e.target.id](e.target.id); 
		}, false);
	}
	
	myForm.addEventListener('submit', function(e) {
	
		var result = true;

		for (var i in check) {
			result = check[i](i) && result;
		}
		if (result==false) {
			e.preventDefault();
		}
				
	}, false);	
})();
