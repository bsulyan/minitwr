function afficheDate(date)
{	
	var jour,nbJour,mois,annee,heure,mine;
	
	switch (date.getDay()){
		case 0 :
			jour="dimanche";
		break;
		case 1 :
			jour="lundi";
		break;
		case 2 :
			jour="mardi";
		break;
		case 3 :
			jour="mercredi";
		break;
		case 4 :
			jour="jeudi";
		break;
		case 5 :
			jour="vendredi";
		break;
		case 6 :
			jour="samedi";
		break;
	}
	nbJour = date.getDate();
	if (nbJour==1)
	{
		nbJour=nbJour+"er";
	}
	
	switch (date.getMonth()){
		case 0 :
			mois = "Janvier";
		break;
		case 1 :
			mois = "Février";
		break;
		case 2 :
			mois = "Mars";
		break;
		case 3 :
			mois = "Avril";
		break;
		case 4 :
			mois = "Mai";
		break;
		case 5 :
			mois = "Juin";
		break;
		case 6 :
			mois = "Juillet";
		break;
		case 7 :
			mois = "Août";
		break;
		case 8 :
			mois = "Septembre";
		break;
		case 9 :
			mois = "Octobre";
		break;
		case 10 :
			mois = "Novembre";
		break;
		case 11 :
			mois = "Décembre";
		break;
	}
	annee=date.getFullYear();
	heure=date.getHours();
	mine=date.getMinutes();
	if(mine < 10)
	{
		mine="0"+mine;
	}
	return "Le "+jour+" "+nbJour+" "+mois+" "+annee+" à "+heure+"h"+mine;
}

function affichePosts(posts, after) {
	
	var section = document.getElementById('posts');
	while (posts.length > 0){
		if(after){
			var post = posts.pop();
		}
		else {
			var post = posts.shift();
		}
		var addPost = document.createElement('blockquote');
		addPost.className = 'twt';

		if (post.hashtag != undefined) {
			for(i=0; i < post.hashtag.length; i++) {
				post.message = post.message.replace('#'+post.hashtag[i],"<a href='/hashtag/undefined?search=" + post.hashtag[i] +"'>#" + post.hashtag[i] + "</a>");
			}
		}

		addPost.innerHTML = "<p><a href='/profil/" + post.pseudo + "'>" + post.pseudo +"</a> a tweet </p><p>"+ post.message + "</p>"+ afficheDate(new Date(post.date)) +"</a>";
		if(after){
			section.appendChild(addPost);
		}
		else {
			section.insertBefore(addPost,section.firstChild);
		}
	}
}

function searchPosts(callback, curs, nb, critere) {
	if(typeof critere === 'undefined'){critere= {}}
	if(typeof critere.pseudo === 'undefined'){critere.pseudo = ''}
	if(typeof critere.hashtag === 'undefined'){critere.hashtag = ''}
	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200 ) {
			callback(JSON.parse(xhr.responseText));
		}
	}
	xhr.open("GET","/searchPosts?curs="+curs+"&nb="+nb+"&pseudo="+critere.pseudo+"&hashtag="+critere.hashtag);
	xhr.send(null);
}

function nbPosts(callback, critere){
	if(typeof critere === 'undefined'){critere= {}}
	if(typeof critere.pseudo === 'undefined'){critere.pseudo = ''}
	if(typeof critere.hashtag === 'undefined'){critere.hashtag = ''}
	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200 ) {
			callback(parseInt(xhr.responseText));
		}
	}
	xhr.open("GET","/nbPosts?pseudo="+critere.pseudo+"&hashtag="+critere.hashtag);
	xhr.send(null);
}
